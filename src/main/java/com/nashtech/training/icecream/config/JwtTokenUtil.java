package com.nashtech.training.icecream.config;

import com.nashtech.training.icecream.models.Admin;
import com.nashtech.training.icecream.models.Customer;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.function.Function;

import static com.nashtech.training.icecream.config.Constants.ACCESS_TOKEN_VALIDITY_SECONDS;
import static com.nashtech.training.icecream.config.Constants.SIGNING_KEY;

@Component
public class JwtTokenUtil implements Serializable {
    public String getUsernameFromToken(String token){
        return getClaimFromToken(token, Claims::getSubject);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }


    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateUserToken(Customer customer) {
        return doGenerateToken(customer.getUsername(),"ROLE_USER", customer.getId().toString());
    }

    public String generateAdminToken(Admin admin) {
        return doGenerateToken(admin.getUsername(),"ROLE_ADMIN", admin.getUsername());
    }

    private String doGenerateToken(String subject, String role, String id) {

        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scopes", new SimpleGrantedAuthority(role));
        claims.put("id", id);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuer("http://ice-cream.com")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000))
                .signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (
                username.equals(userDetails.getUsername())
                        && !isTokenExpired(token));
    }
}
