package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.OnlineOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OnlineOrderRepository extends JpaRepository<OnlineOrder, Integer> {
}
