package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.Faq;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FaqRepository  extends JpaRepository<Faq, Integer> {
}
