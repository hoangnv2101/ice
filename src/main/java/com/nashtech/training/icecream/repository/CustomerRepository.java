package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

 /*   @Query("select u from Customer u where u.username = :username")*/
    Customer findByUsername(String username);
}
