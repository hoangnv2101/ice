package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, String> {
}
