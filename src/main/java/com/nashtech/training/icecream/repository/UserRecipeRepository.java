package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.UserRecipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRecipeRepository  extends JpaRepository<UserRecipe, Integer> {

}
