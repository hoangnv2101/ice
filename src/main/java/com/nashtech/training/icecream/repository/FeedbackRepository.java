package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
}
