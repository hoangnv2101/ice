package com.nashtech.training.icecream.repository;

import com.nashtech.training.icecream.models.Recipe;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Integer> {
    @Query(value = "select r from Recipe r")
    List<Recipe> findWithPageable(Pageable pageable);
}
