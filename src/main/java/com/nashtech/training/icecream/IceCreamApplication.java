package com.nashtech.training.icecream;

import com.nashtech.training.icecream.service.impl.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableConfigurationProperties({
		StorageProperties.class
})
public class IceCreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(IceCreamApplication.class, args);
	}


}
