package com.nashtech.training.icecream.models;

import com.nashtech.training.icecream.utils.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name="faq")
public class Faq implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "faq_id")
    private Integer id;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "question may be null or empty.")
    @Column(name = "question")
    private String question;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "answer may be null or empty.")
    @Column(name = "answer")
    private String answer;

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
