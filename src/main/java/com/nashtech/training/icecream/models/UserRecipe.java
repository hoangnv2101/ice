package com.nashtech.training.icecream.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nashtech.training.icecream.utils.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "user_recipe")
public class UserRecipe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_recipe_id")
    private Integer id;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "name may be null or empty.")
    @Size(max = 100)
    @Column(name = "name")
    private String name;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "image may be null or empty.")
    @Size(max = 100)
    @Column(name = "image")
    private String image;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "desciption may be null or empty.")
    @Column(name = "desciption")
    private String desciption;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "details may be null or empty.")
    @Column(name = "details")
    private String details;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "prize_status status may be null or empty.")
    @Column(name = "prize_status")
    private Boolean prizeStatus;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "enable status may be null or empty.")
    @Column(name = "enable_status")
    private Boolean enablestatus;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Boolean getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(Boolean prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public Boolean getEnablestatus() {
        return enablestatus;
    }

    public void setEnablestatus(Boolean enablestatus) {
        this.enablestatus = enablestatus;
    }
}
