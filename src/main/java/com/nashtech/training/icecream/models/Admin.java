package com.nashtech.training.icecream.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="admin")
public class Admin  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

