package com.nashtech.training.icecream.models;

import com.nashtech.training.icecream.utils.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_id_seq")
    @SequenceGenerator(name = "customer_id_seq", sequenceName = "customer_id_seq")
    @Column(name = "customer_id")
    private Integer id;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "user name may be null or empty.")
    @Size(max = 100)
    @Column(name = "username",unique = true)
    private String username;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "password may be null or empty.")
    @Column(name = "password")
    private String password;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "full name may be null or empty.")
    @Size(max = 100)
    @Column(name = "full_name")
    private String fullName;

    @Size(max = 100)
    @Column(name = "address")
    private String address;

    @Size(max = 50)
    @Column(name = "phone_number")
    private String phoneNumber;

    @Size(max = 100)
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "gender")
    private Boolean gender;

    @Column(name = "birthday")
    private Date birthDay;

    @Size(max = 2048)
    @Column(name = "avatar")
    private String avatar;

    @Column(name = "expired_date")
    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "expired Date may be null.")
    private Date expiredDate = new Date();

    @Column(name = "enable_status", columnDefinition = "boolean default true", nullable = false)
    private Boolean enableStatus = true;

/*    @OneToMany(mappedBy = "customer")
    private Set<UserRecipe> items = new HashSet<>();*/

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Boolean getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Boolean enableStatus) {
        this.enableStatus = enableStatus;
    }
}
