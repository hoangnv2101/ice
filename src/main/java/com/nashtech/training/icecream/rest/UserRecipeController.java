package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.Customer;
import com.nashtech.training.icecream.models.UserRecipe;
import com.nashtech.training.icecream.repository.UserRecipeRepository;
import com.nashtech.training.icecream.service.CustomerService;
import com.nashtech.training.icecream.service.StorageService;
import com.nashtech.training.icecream.service.impl.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

@RestController
public class UserRecipeController extends BaseController {

    @Autowired
    private UserRecipeRepository userRecipeRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    StorageProperties storageProperties;


    @PostMapping(value = "/user-recipe", headers = "Content-Type=multipart/form-data")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public UserRecipe createUserRecipe(@RequestParam(value = "file", required = false) MultipartFile file, @RequestPart("customer") UserRecipe userRecipe, Principal principal) {
        Customer customer = this.customerService.findByUsername(principal.getName());
        userRecipe.setCustomer(customer);
        UserRecipe cus = userRecipeRepository.save(userRecipe);
        if (file != null) {
            storageService.store(file, storageProperties.getUserrecipe());
        }
        return cus;
    }


    @GetMapping(value = "/user-recipe")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<UserRecipe> getAllUserRecipes() {
        List<UserRecipe> list = userRecipeRepository.findAll();
        return list;
    }

    @DeleteMapping(value = "/user-recipe/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void delete(@PathVariable Integer id) {
        UserRecipe userRecipe = userRecipeRepository.findById(id).get();
        userRecipeRepository.delete(userRecipe);
    }

    @GetMapping(value = "/user-recipe/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public UserRecipe getUserRecipe(@PathVariable Integer id) {
        UserRecipe userRecipe = userRecipeRepository.findById(id).get();
        return userRecipe;
    }

    @PatchMapping("/user-recipe/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public UserRecipe updateStatus(@PathVariable Integer id, @RequestBody UserRecipe userRecipe) {
        UserRecipe recipe = userRecipeRepository.findById(id).get();
        recipe.setPrizeStatus(userRecipe.getPrizeStatus());
        return userRecipeRepository.save(recipe);
    }
}
