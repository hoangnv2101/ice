package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.ChangePassword;
import com.nashtech.training.icecream.models.Customer;
import com.nashtech.training.icecream.repository.CustomerRepository;
import com.nashtech.training.icecream.service.CustomerService;
import com.nashtech.training.icecream.service.StorageService;
import com.nashtech.training.icecream.service.impl.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
public class CustomerController extends BaseController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private StorageService storageService;

    @Autowired
    StorageProperties storageProperties;

    @PostMapping(value = "/customers", headers = "Content-Type=multipart/form-data")
    public Customer createCustomer(@RequestParam(value = "file", required = false) MultipartFile file, @RequestPart("customer") Customer customer) {

        customer.setPassword(new BCryptPasswordEncoder().encode(customer.getPassword()));
        Customer cus = customerRepository.save(customer);
        if (file != null) {
            storageService.store(file, storageProperties.getUploaded());
        }
        return cus;
    }

    @PatchMapping(value = "/customers", headers = "Content-Type=multipart/form-data")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public Customer updateCustomer(@RequestParam(value = "file", required = false) MultipartFile file, @RequestPart("customer") Customer customer, Principal principal) {
        customer.setUsername(principal.getName());
        Customer cus = customerService.updateCustomer(customer);
        if (file != null) {
            storageService.store(file, storageProperties.getUploaded());
        }
        return cus;
    }

    @PatchMapping(value = "/customers/{id}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public Customer update(@PathVariable Integer id, @RequestBody Customer customer) {

        String username = customerRepository.findById(id).get().getUsername();
        customer.setUsername(username);
        Customer cus = customerService.updateCustomer(customer);
        return cus;
    }

    @PatchMapping(value = "/customers/update")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void update(@RequestBody List<Customer> customers) {
        customerService.updateCustomers(customers);

    }

    @GetMapping("/customers")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<Customer> findAllCustomers() {
        return customerRepository.findAll();
    }

    @GetMapping("/customers/{id}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public Customer getCustomer(@PathVariable Integer id) {
        return customerRepository.findById(id).get();
    }

    @PostMapping("/customers/changePassword")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        this.customerService.changPassword(changePassword);
    }
}
