package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.Faq;
import com.nashtech.training.icecream.repository.FaqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FaqController extends BaseController{

    @Autowired
    private FaqRepository faqRepository;

    @GetMapping("/faq")
    public List<Faq> findAll() {
        return faqRepository.findAll();
    }

    @PostMapping("/faq")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Faq createFaq(@RequestBody Faq faq) {
        return faqRepository.save(faq);
    }

    @DeleteMapping("/faq/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void delete(@PathVariable Integer id) {
        Faq faq = faqRepository.findById(id).get();
        faqRepository.delete(faq);
    }
}
