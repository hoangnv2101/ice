package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.exception.ResourceNotFoundException;
import com.nashtech.training.icecream.models.Recipe;
import com.nashtech.training.icecream.repository.RecipeRepository;
import com.nashtech.training.icecream.service.RecipeService;
import com.nashtech.training.icecream.service.StorageService;
import com.nashtech.training.icecream.service.impl.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class RecipeController extends BaseController {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private StorageService storageService;

    @Autowired
    StorageProperties storageProperties;

    @GetMapping("/recipes")
    public List<Recipe> createCustomer() {
        return recipeRepository.findAll();
    }

    @GetMapping("/recipes/{id}")
    public Recipe getRecipe(@PathVariable Integer id) {
        return recipeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + id));
    }

    @PatchMapping(value = "/recipes")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Recipe update(@RequestParam(value = "file", required = false) MultipartFile file, @RequestPart("recipe") Recipe recipe) {

        Recipe cus = recipeService.update(recipe);

        if (file != null) {
            storageService.store(file, "");
        }

        return cus;
    }

    @PutMapping("/recipes/{id}/view")
    public Recipe upDateView(@PathVariable Integer id) {
        Recipe recipe = recipeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + id));
        recipe.setViewNumber(recipe.getViewNumber() + 1);
        return recipeRepository.save(recipe);
    }

    @GetMapping(value = "/recipes/new", params = {"maxResult"})
    public List<Recipe> getTopNew(@RequestParam(name = "maxResult") int maxResult) {
        List<Recipe> recipes = recipeRepository.findWithPageable(PageRequest.of(0, 10, Sort.Direction.DESC, "id"));
        return recipes;
    }

    @GetMapping(value = "/recipes/view", params = {"maxResult"})
    public List<Recipe> getTopView(@RequestParam(name = "maxResult") int maxResult) {
        List<Recipe> recipes = recipeRepository.findWithPageable(PageRequest.of(0, 10, Sort.Direction.DESC, "viewNumber"));
        return recipes;
    }

    @PostMapping("/recipes")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Recipe createRecipe(@RequestParam(value = "file", required = false) MultipartFile file,
                               @RequestPart("recipe") Recipe recipe) {
        Recipe re = this.recipeRepository.save(recipe);
        if (file != null) {
            storageService.store(file, "");
        }
        return re;
    }

}


