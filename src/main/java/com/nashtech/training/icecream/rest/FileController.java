package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.service.StorageService;
import com.nashtech.training.icecream.service.impl.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

@RestController
public class FileController extends BaseController {
    private final StorageService storageService;
    private final String uploaded;
    private final String userrecipe;

    @Autowired
    public FileController(StorageService storageService, StorageProperties storageProperties) {
        this.storageService = storageService;
        this.uploaded = storageProperties.getUploaded();
        this.userrecipe = storageProperties.getUserrecipe();
    }

    @GetMapping("/files")
    public Stream<Path> listUploadedFiles() throws IOException {

        return this.storageService.loadAll("");
    }

    @PostMapping("/avatar/uploadFile")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        storageService.store(file, this.uploaded);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/images/uploaded/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = this.storageService.loadAsResource(fileName, this.uploaded);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


    @GetMapping("/images/{fileName:.+}")
    public ResponseEntity<Resource> getRecipe(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = this.storageService.loadAsResource(fileName, "");

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping("/images/userrecipe/{fileName:.+}")
    public ResponseEntity<Resource> getUserRecipe(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = this.storageService.loadAsResource(fileName, this.userrecipe);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
