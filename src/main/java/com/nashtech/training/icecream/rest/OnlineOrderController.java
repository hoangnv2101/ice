package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.OnlineOrder;
import com.nashtech.training.icecream.repository.OnlineOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class OnlineOrderController extends BaseController {

    @Autowired
    OnlineOrderRepository onlineOrderRepository;

    @PostMapping("/orders")
    public OnlineOrder createOnlineOrder(@RequestBody OnlineOrder onlineOrder) {
        return onlineOrderRepository.save(onlineOrder);
    }

    @GetMapping("/orders")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<OnlineOrder> getAll() {
        return onlineOrderRepository.findAll();
    }


    @GetMapping("/orders/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public OnlineOrder getOrder(@PathVariable Integer id) {
        return onlineOrderRepository.getOne(id);
    }

    @PatchMapping("/orders/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public OnlineOrder updateStatus(@PathVariable Integer id, @RequestBody OnlineOrder order) {
        OnlineOrder onlineOrder = onlineOrderRepository.findById(id).get();
        onlineOrder.setStatus(order.getStatus());
        return onlineOrderRepository.save(onlineOrder);
    }
}
