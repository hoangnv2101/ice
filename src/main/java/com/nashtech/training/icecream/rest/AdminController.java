package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.ChangePassword;
import com.nashtech.training.icecream.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController extends BaseController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/admin/changePassword")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        this.adminService.changPassword(changePassword);
    }

}
