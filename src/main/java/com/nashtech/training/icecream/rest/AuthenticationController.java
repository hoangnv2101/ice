package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.config.JwtTokenUtil;
import com.nashtech.training.icecream.models.Admin;
import com.nashtech.training.icecream.models.AuthToken;
import com.nashtech.training.icecream.models.Customer;
import com.nashtech.training.icecream.models.LoginUser;
import com.nashtech.training.icecream.repository.AdminRepository;
import com.nashtech.training.icecream.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class AuthenticationController extends BaseController{

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AdminRepository adminRepository;

    @RequestMapping(value = "/token/generate-token", method = RequestMethod.POST)
    public ResponseEntity generateToken(@RequestBody LoginUser loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<Admin> admin = adminRepository.findById(loginUser.getUsername());
        String token;

        if (admin.isPresent()) {
            token = jwtTokenUtil.generateAdminToken(admin.get());
        } else {
            final Customer user = customerRepository.findByUsername(loginUser.getUsername());
            token = jwtTokenUtil.generateUserToken(user);
        }
        return ResponseEntity.ok(new AuthToken(token));
    }
}
