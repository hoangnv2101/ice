package com.nashtech.training.icecream.rest;

import com.nashtech.training.icecream.models.Feedback;
import com.nashtech.training.icecream.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FeedbackController extends BaseController {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @PostMapping("/feedback")
    public Feedback createOnlineOrder(@RequestBody Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    @GetMapping("/feedback")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<Feedback> getAll() {
        return feedbackRepository.findAll();
    }

    @DeleteMapping("/feedback/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void delete(@PathVariable Integer id) {
        Feedback feedback = feedbackRepository.findById(id).get();
        feedbackRepository.delete(feedback);
    }
}
