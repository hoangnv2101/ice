package com.nashtech.training.icecream.utils;

/**
 * This is common util that defines constant and util functions.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public abstract class ApiUtil
{
    /* Route Constants */
    public static final String ROOT_URL = "/rest";

    public static final String CUSTOMER_URL = ROOT_URL + "/customers";
    public static final String RECIPE_URL = ROOT_URL + "/recipes";
    public static final String ONLINE_ORDER_URL = ROOT_URL + "/orders";
    public static final String RECIPE_TOP_VIEW_URL =  "/view";
    public static final String RECIPE__TOP_NEW_URL =  "/new";
    public static final String ID_URL = "/{id}";
    public static final String SEARCH_URL = "/search";

    public static final String HTTP_STATUS = "http_status";

    public static final String SEARCH_TEXT = "search_text";
    public static final String SORT_BY = "sort_by";

    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final Integer DEFAULT_PAGE = 0;
    public static final Integer DEFAULT_PERPAGE = 25;

    public static final String COLLECTION_META_TYPE = "collection";

    public static final String ERROR_CODE_SPLIT = "#";
    public static final String QUESTION_MARK_SPLIT = "?";
    public static final String GENERAL_REC = "resource_error";

    public static final String BLANK_REC = "blank";
    public static final String INVALID_TYPE_REC = "invalid_type";
    public static final String INVALID_TYPE_REC_MSG = "Field may be invalid type.";
    public static final String INVALID_VALUE_REC_MSG = "Field may be invalid value.";
    /* RESOURCE ERROR CODES: RSC */


    public static final String UNKNOWN_REC = "unknown";

    public static final String INCORRECT_VALUE_REC = "incorrect_value";
    public static final String UNKNOWN_REC_MSG = "Field may be not found.";

}
