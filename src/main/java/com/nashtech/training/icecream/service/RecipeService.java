package com.nashtech.training.icecream.service;

import com.nashtech.training.icecream.models.Recipe;

public interface RecipeService {
    Recipe update(Recipe recipe);
}
