package com.nashtech.training.icecream.service;

import com.nashtech.training.icecream.models.Admin;
import com.nashtech.training.icecream.models.ChangePassword;

public interface AdminService {
    Admin findByUsername(String username);
    void  changPassword(ChangePassword changePassword);
}
