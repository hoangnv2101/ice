package com.nashtech.training.icecream.service;

import com.nashtech.training.icecream.models.ChangePassword;
import com.nashtech.training.icecream.models.Customer;

import java.util.List;

public interface CustomerService {
    Customer findByUsername(String username);

    Customer updateCustomer(Customer customer);

    void updateCustomers(List<Customer> customers);

    void  changPassword(ChangePassword changePassword);
}
