package com.nashtech.training.icecream.service.impl;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location;
    private String uploaded;
    private String userrecipe;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }

    public String getUserrecipe() {
        return userrecipe;
    }

    public void setUserrecipe(String userrecipe) {
        this.userrecipe = userrecipe;
    }
}
