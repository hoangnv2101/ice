package com.nashtech.training.icecream.service.impl;

import com.nashtech.training.icecream.exception.PasswordNotMatchException;
import com.nashtech.training.icecream.models.Admin;
import com.nashtech.training.icecream.models.ChangePassword;
import com.nashtech.training.icecream.models.Customer;
import com.nashtech.training.icecream.repository.AdminRepository;
import com.nashtech.training.icecream.repository.CustomerRepository;
import com.nashtech.training.icecream.service.CustomerService;
import com.nashtech.training.icecream.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
public class CustomerServiceImpl implements UserDetailsService, CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Override
    public Customer findByUsername(String username) {
        return customerRepository.findByUsername(username);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        Customer existing;
        if (customer.getId() != null) {
            existing = customerRepository.findById(customer.getId()).get();
        } else {
            existing = customerRepository.findByUsername(customer.getUsername());
        }
        CommonUtils.copyNonNullProperties(customer, existing);
        return customerRepository.save(existing);
    }

    @Override
    public void updateCustomers(List<Customer> customers) {
        for (Customer cus : customers) {
            updateCustomer(cus);
        }
    }

    @Override
    public void changPassword(ChangePassword changePassword) {

        if (changePassword.getPassword() == null || changePassword.getOldPass() == null) {
            throw new PasswordNotMatchException("The new password or old password must be not empty");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        PasswordEncoder token = new BCryptPasswordEncoder();
        boolean isMatch = token.matches(changePassword.getOldPass(), user.getPassword());
        if (!isMatch) {
            throw new PasswordNotMatchException("The password does not match with current password");
        }

        Customer cus = this.customerRepository.findByUsername(user.getUsername());
        cus.setPassword(token.encode(changePassword.getPassword()));
        this.customerRepository.save(cus);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Admin> admin = adminRepository.findById(username);

        if (!admin.isPresent()) {
            Customer customer = customerRepository.findByUsername(username);
            if (customer == null) {
                throw new UsernameNotFoundException("Invalid username or password.");
            }
            return new org.springframework.security.core.userdetails.User(customer.getUsername(), customer.getPassword(), getAuthority("ROLE_USER"));
        } else {
            return new org.springframework.security.core.userdetails.User(admin.get().getUsername(), admin.get().getPassword(), getAuthority("ROLE_ADMIN"));
        }
    }

    private List<SimpleGrantedAuthority> getAuthority(String role) {
        return Arrays.asList(new SimpleGrantedAuthority(role));
    }
}
