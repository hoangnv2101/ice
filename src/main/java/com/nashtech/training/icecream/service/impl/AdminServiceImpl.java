package com.nashtech.training.icecream.service.impl;

import com.nashtech.training.icecream.exception.PasswordNotMatchException;
import com.nashtech.training.icecream.models.Admin;
import com.nashtech.training.icecream.models.ChangePassword;
import com.nashtech.training.icecream.repository.AdminRepository;
import com.nashtech.training.icecream.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service(value = "adminService")
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminRepository adminRepository;

    @Override
    public Admin findByUsername(String username) {
        return adminRepository.findById(username).get();
    }

    @Override
    public void changPassword(ChangePassword changePassword) {

        if (changePassword.getPassword() == null || changePassword.getOldPass() == null) {
            throw new PasswordNotMatchException("The new password or old password must be not empty");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        PasswordEncoder token = new BCryptPasswordEncoder();
        boolean isMatch = token.matches(changePassword.getOldPass(), user.getPassword());
        if (!isMatch) {
            throw new PasswordNotMatchException("The password does not match with current password");
        }

        Admin cus = findByUsername(user.getUsername());
        cus.setPassword(token.encode(changePassword.getPassword()));
        this.adminRepository.save(cus);
    }
}
