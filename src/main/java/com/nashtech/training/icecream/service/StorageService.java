package com.nashtech.training.icecream.service;

import org.springframework.web.multipart.MultipartFile;

import org.springframework.core.io.Resource;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {
    void init();

    void store(MultipartFile file, String childFolder);

    Stream<Path> loadAll(String childFolder);

    Path load(String filename, String childFolder);

    Resource loadAsResource(String filename, String childFolder);

    void deleteAll();
}
