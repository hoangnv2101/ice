package com.nashtech.training.icecream.service.impl;

import com.nashtech.training.icecream.models.Recipe;
import com.nashtech.training.icecream.repository.RecipeRepository;
import com.nashtech.training.icecream.service.RecipeService;
import com.nashtech.training.icecream.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "recipeService")
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Override
    public Recipe update(Recipe recipe) {
        if (recipe.getId() == null) {
            return null;
        }
        Recipe existing = recipeRepository.findById(recipe.getId()).get();
        CommonUtils.copyNonNullProperties(recipe, existing);
        return recipeRepository.save(existing);
    }
}
